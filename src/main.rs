// use dynamodb aws sdk to interact with db
use aws_sdk_dynamodb::model::AttributeValue;
use aws_sdk_dynamodb::Client;
use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use rand::Rng;
use serde::{Deserialize, Serialize};
use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use tracing::log::info;

// data to push to the dynamodb table
#[derive(Debug, Serialize, Deserialize)]
pub struct Quote {
    pub quote_id: String,
    pub quote_value: String,
}

// required for AWS
#[derive(Debug, Serialize)]
struct FailureResponse {
    pub body: String,
}

impl std::fmt::Display for FailureResponse {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.body)
    }
}

#[derive(Deserialize)]
struct Request {
    command: String,
}

#[derive(Serialize, Debug)]
struct Response {
    req_id: String,
    msg: String,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let r_id = event.context.request_id;
    let config = aws_config::load_from_env().await;
    let client = Client::new(&config);

    // Define quotes
    let quotes = [
        "The only way to do great work is to love what you do. -Steve Jobs",
        "Life is what happens when you're busy making other plans. -John Lennon",
        "Get busy living or get busy dying. -Stephen King",
        "It does not matter how slowly you go as long as you do not stop. -Confucius",
    ];

    // Randomly select a quote based on the command
    let mut rng = rand::thread_rng();
    let quote_index: usize = rng.gen_range(0..quotes.len()); // generates a number between 0 and 3
    let selected_quote = quotes[quote_index];

    if event.payload.command == "add" {
        // Prepare to store value
        let quote_id = AttributeValue::S(r_id.clone());
        let quote_value = AttributeValue::S(selected_quote.to_string());

        // Add to DynamoDB
        let _resp = client
            .put_item()
            .table_name("quote")
            .item("quote_id",quote_id )
            .item("quote_value", quote_value)
            .send()
            .await
            .map_err(|_err| FailureResponse {
                body: _err.to_string(),
            });
    

        // Prepare the response
        let resp = Response {
            req_id: r_id,
            msg: format!("Quote added: {:?}", selected_quote),
        };
        // Log info about the response
        info!("Response: {:?}", resp);

        Ok(resp)
    } else {
        let resp = Response {
            req_id: r_id,
            msg: format!("Not supported commnad"),
        };
        // Log info about the response
        info!("Response: {:?}", resp);

        Ok(resp)

    }
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .with_target(false)
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}

#[cfg(test)]
mod tests {
    use crate::{function_handler, Request};
    use lambda_runtime::{Context, LambdaEvent};

    #[tokio::test]
    async fn response_is_good_for_simple_input() {
        let id = "123456789";

        let mut context = Context::default();
        context.request_id = id.to_string();

        let payload = Request {
            command: "add".to_string(),
        };
        let event = LambdaEvent { payload, context };

        let result = function_handler(event).await.expect("Expected Ok response");

        assert!(result.msg.contains("Quote added:"));
    }
}
