# tinayiluo_IDS721_week6

[![pipeline status](https://gitlab.com/ly178/tinayiluo-ids-721-week-6/badges/main/pipeline.svg)](https://gitlab.com/ly178/tinayiluo-ids-721-week-6/-/commits/main)

## Purpose
This project is dedicated to developing a serverless application using AWS Lambda, programmed in Rust with Cargo Lambda. It not only integrates AWS Lambda with AWS DynamoDB to deliver a "Quote of the Day" service but also emphasizes on advanced monitoring and observability practices. By incorporating logging, AWS X-Ray tracing, and CloudWatch, the application showcases not just the seamless integration of AWS Lambda, AWS API Gateway, and DynamoDB, but also the efficiency and scalability of serverless architecture with robust monitoring and diagnostics capabilities. This enhancement aims to provide deeper insights into the application's performance, facilitating easier debugging and performance tuning.

## Introduction
At the core of this project lies a Lambda function that generates and delivers a random quote of the day to users, subsequently storing this quote in a DynamoDB table. This fundamental functionality is augmented with the implementation of comprehensive logging and AWS X-Ray tracing, underpinning the importance of observability in modern cloud-native applications. Through this project, users are introduced to the practical use of serverless computing for dynamic content generation, database interaction, and the critical role that monitoring and tracing play in maintaining and optimizing application performance.

## Usability
Interacting with the Lambda function is straightforward, allowing users to invoke the function and receive a new quote through `make aws-invoke` (provided appropriate credentials are available) or via a dedicated API Gateway link. The integration of AWS CloudWatch and AWS X-Ray into the application not only ensures that it remains accessible and easy to use for those seeking motivational quotes but also that it offers transparent, centralized logging and tracing. This added layer of instrumentation enables users, developers, and administrators to track the application's performance, troubleshoot issues efficiently, and ensure the service runs smoothly at all times.

## Requirements
- **Development Environment**: Installation of Rust and Cargo Lambda is necessary, achievable through Homebrew commands: `brew install rust`, `brew tap cargo-lambda/cargo-lambda`, and `brew install cargo-lambda`.
- **AWS Account**: Users must have an AWS account to deploy and manage the Lambda function.

## Setup and Deployment
1. Initialize a new Cargo Lambda project: `cargo lambda new new-lambda-project`.
2. Incorporate required dependencies into `Cargo.toml`.
3. Implement quote generation logic within `src/main.rs`.
4. Add Logging in quote generation within `src/main.rs`
```
// Log info about the response
info!("Response: {:?}", resp);
```
5. Local testing can be done using `cargo lambda watch`.
6. Invoke the Lambda function locally for testing with `cargo lambda invoke --data-ascii "{ \"command\": \"add\"}"`.
7. Utilize the AWS IAM console to create an IAM User for deployment.
8. Attach necessary policies (`lambdafullaccess` and `iamfullaccess`).
9. Generate access key in security credentials (AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_REGION (us-east-1)).
10. Securely store the generated access key in a `.env` file, ensuring it's excluded from version control.
11. Facilitate deployment by exporting AWS credentials to Cargo Lambda. 

```bash
set -a # Automatically export all variables
source .env
set +a
```

11. Complete the build and deployment process using `cargo lambda build --release` and `cargo lambda deploy`.
12. Adjust Lambda function settings in AWS to permit DynamoDB access (`AWSDynamoDBFullAccess`).
13. Establish a DynamoDB table named "quote" with "quote_id" as the primary key.
14. Verify function effectiveness by ensuring DynamoDB reflects new entries.
15. Integrate the function with AWS API Gateway for HTTP access.
16. Finalize API deployment and note the invocation URL for user access.

### Integration with API Gateway
After deploying the API, navigate to `stages` within the AWS console to locate the invoke URL, such as:

```
https://oe1rvmzzl5.execute-api.us-east-1.amazonaws.com/quote/grab
```

#### Testing the API Gateway
Verify API functionality using Postman or via a curl request:

```bash
curl -X POST https://hh7sc6r1vl.execute-api.us-east-1.amazonaws.com/coin/flip \
  -H 'content-type: application/json' \
  -d '{ "command": "add"}'
```

#### Testing the Lambda Function Directly
For direct Lambda function testing, bypassing API Gateway:

```bash
cargo lambda invoke --remote tinayiluo_ids721_week5mini --data-ascii "{ \"command\": \"add\"}"
```

## Continuous Integration and Deployment
Ensure AWS credentials (`AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_REGION`) are securely added to GitLab secrets for CI/CD automation. Utilize `.gitlab-ci.yml` and a `Makefile` for streamlined builds, tests, and deployments.

## Logging and Monitoring

To ensure comprehensive insights into the Lambda function's performance and to help troubleshoot issues, follow these steps to enable and configure logging and monitoring services:

### Enable CloudWatch Logs

AWS Lambda functions automatically integrate with Amazon CloudWatch Logs. If not already enabled by default, activate CloudWatch for the Lambda function through the following steps:

1. Navigate to the Lambda function in the AWS Management Console.
2. Click on the "Monitoring" tab.
3. If CloudWatch Logs are not enabled, AWS will prompt to enable them. Follow the provided instructions.

### Activate X-Ray Tracing

AWS X-Ray helps to analyze and debug distributed applications. To enable X-Ray tracing for the Lambda function:

1. Go to the Lambda function in the AWS Management Console.
2. Select "Configuration" tab.
3. Under "Monitoring and operations tools," find the "Active tracing" option and toggle it to enable X-Ray tracing.

### Viewing Logs in CloudWatch

To view the Lambda function logs:

1. Access the Amazon CloudWatch console.
2. Navigate to "Logs" and then "Log groups."
3. Locate and select the log group for the Lambda function (usually named `/aws/lambda/<function-name>`).
4. Here, we can view and search through the function's centralized logs.

### Integrate API Gateway with CloudWatch Logs

For applications using Amazon API Gateway, further enhance the monitoring capabilities by enabling CloudWatch Logs for the API Gateway:

1. Open the IAM (Identity and Access Management) console.
2. Create a new role with permissions to access CloudWatch. To do this:
   - Click on "Roles" then "Create role."
   - Choose "AWS service" for the type of trusted entity and select "API Gateway."
   - Attach the CloudWatch policy to this role (search for CloudWatch permissions).
   - Name the role and create it.
   - Copy the ARN (Amazon Resource Name) of the newly created role.
3. Navigate to the API Gateway console.
4. Select the API Gateway.
5. Under "General settings," paste the ARN of the IAM role into the CloudWatch log role ARN field.
6. Go to "Stages" for the specific API Gateway.
7. Enable CloudWatch logging and select the logging level and data trace settings that we are interested in.

By following these steps, there will be detailed logging and monitoring setup for both the AWS Lambda functions and API Gateway, allowing for effective tracking and debugging of the application's performance and issues.

## Deliverables
This project successfully delivers an operational Lambda function, documented tests, a DynamoDB table setup for data storage, API Gateway integration for function invocation, comprehensive test results via Postman, and an advanced logging mechanism. 

### Logging 

![Screen Shot 2024-03-06 at 11.46.44 PM.png](/uploads/a927f1fc8609c0baa85747b67f994404/Screen_Shot_2024-03-06_at_11.46.44_PM.png)

### Lambda function 

![Screen_Shot_2024-03-07_at_1.11.50_AM](/uploads/be2f9c5b3fe7da4d6b0a9d3ad9faafc3/Screen_Shot_2024-03-07_at_1.11.50_AM.png)

### Lambda Test 

![Screen_Shot_2024-03-07_at_1.09.58_AM](/uploads/6851e221abd761ed3dd31d59468fa774/Screen_Shot_2024-03-07_at_1.09.58_AM.png)

### DynamoDB table

![Screen_Shot_2024-03-07_at_1.07.30_AM](/uploads/fbf50e641f2a03ca0ca3cb4c0a94a773/Screen_Shot_2024-03-07_at_1.07.30_AM.png)

### API gateway

![Screen_Shot_2024-03-07_at_1.05.51_AM](/uploads/cf8c707595fd7817921fbc9ed07e5908/Screen_Shot_2024-03-07_at_1.05.51_AM.png)

### Postman Test

![Screen_Shot_2024-03-07_at_1.04.26_AM](/uploads/875a23c4a8cbc364fb93efdf10512b75/Screen_Shot_2024-03-07_at_1.04.26_AM.png)